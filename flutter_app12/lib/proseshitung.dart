import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HitungVolumeKubus extends StatefulWidget {
  @override
  _HitungVolumeKubusState createState() => _HitungVolumeKubusState();
}

class _HitungVolumeKubusState extends State<HitungVolumeKubus> {
  double sisi1=0;
  double sisi2=0;
  double sisi3=0;
  double volume=0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("MENGHITUNG VOLUME KUBUS", style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 40),),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Color(0xffffffff), Color(0xffff00e2)],
                  begin: FractionalOffset.topLeft,
                  end: FractionalOffset.bottomCenter,
            )
          ),
        ),
      ),

      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              width: double.infinity,
              height: 200,
              margin: EdgeInsets.all(16),
              padding: EdgeInsets.all(16),
              color: Colors.lightBlueAccent,
            child: Image.asset("gambar/kubus.gif", fit: BoxFit.contain,),
          ),
          Text("Hitung Volume Kubus", style: TextStyle(fontWeight: FontWeight.bold),),
          Row(
            children: [
              Text("      SISI RUSUK (r1)   : "),

              Expanded(
                child: TextField(
                  onChanged: (txt){
                    setState(() {
                      sisi1= double.parse(txt);
                    });
                  },
                  keyboardType: TextInputType.number,

                ),
              )
            ],
          ),
          Row(
            children: [
              Text("      SISI RUSUK (r2)   : " ),

              Expanded(
                child: TextField(
                  onChanged: (txt){
                    setState(() {
                      sisi2= double.parse(txt);
                    });
                  },
                  keyboardType: TextInputType.number,

                ),
              )
            ],
          ),
          Row(
            children: [
              Text("      SISI RUSUK (r3:)    : "),
              Expanded(
                child: TextField(
                  onChanged: (txt){
                    setState(() {
                      sisi3= double.parse(txt);
                    });
                  },
                  keyboardType: TextInputType.number,

                ),
              )
            ],
          ),
          // ignore: deprecated_member_use
          RaisedButton(
              onPressed: (){
                setState(() {
                  volume=  sisi1 * sisi2 * sisi3;
                });
              },
              child: Text("HITUNG"),
          ),
        
          Text("VOLUME NYA ADALAH   :$volume",style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 20),)
        ],
      ),
    );
  }
}